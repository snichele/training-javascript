if(typeof ORLANDO === 'undefined') alert("Le fichier 'main' doit être inclus avant 'ui' !");
if(typeof _ === 'undefined') alert("La librairie underscore.js doit être inclus avant 'ui' !");
if(typeof ORLANDO.Utils === 'undefined') alert("Le fichier 'utils' doit être inclus avant 'ui' !");
if(typeof ORLANDO.Model === 'undefined') alert("Le fichier 'model' doit être inclus avant 'ui' !");
if(typeof ORLANDO.DAO === 'undefined') alert("Le fichier 'dao' doit être inclus avant 'ui' !");

/** Contient l'ensemble des objets destinés à gérer la dynamique de l'interface utilisateur. */
ORLANDO.namespace("UI");

/** Workspace est un singleton contenant les méthodes générales de manipulation de l'interface.*/
ORLANDO.UI.Workspace =  (function () {
    /** L'objet workspace.*/
    var that = {};

    /** Contient des informations de positionnement des éléments mobiles de la page. */
    that.positions = {};

    /** Calcul centralisée des différentes positions des éléments mobiles de la page.
     *  A appeller systématiquement avant chaque animation de déplacement.
     **/
    that.computePositions = function () {
        var wWidth = $(window).width(), wHeight = $(window).height();
        this.positions = {
            welcome : {
                hidden : -wWidth,
                visible : 0
            },
            spectacle : {
                hidden : -wWidth,
                visible : 0
            },
            programmation : {
                hidden : -wWidth,
                visible : 0
            },
            nav : {
                'hidden-padding-top' : "-150px",
                'visible-padding-top': "1em"
            },
            header : {
                'hidden-bottom' : "-150px",
                'visible-bottom': "0px"
            }
        };
    };

    /** Les différents états principaux que l'interface peut prendre. */
    that.states = ["empty","greeting", "visible-workspace"];

    /** Change l'état de l'interface. */
    that.changeState = function (state) {
        ORLANDO.UI.Workspace.computePositions();

        if(state == this.states[0]){
            $("nav, body > header").css('visibility', 'hidden');
            $("#welcome, #spectacle, #programmation").css("left", -$(window).width());
        } else
        if(state == this.states[1]){
            that.nicelyDisplaySection("welcome");
            // populate empty part of the ui with default value
            ORLANDO.UI.Movies.updateListWith([]);
            ORLANDO.UI.Movies.updateMovieDetailWith(null);
        } else
        if(state == this.states[2]){
            $("nav, body > header").css('visibility', 'visible');
            that.nicelyDisplaySection("spectacle");
        }
    }

    that.nicelyDisplaySection =  function (which) {
        var that = this;
        var sections = ["welcome","spectacle","programmation"];
        // Cache les autres sections et désurbille (?) les liens cliquables associés
        _.map(_.without(sections, which), function (next) {
            $('#'+ next).stop().animate({
                left: that.positions[next].hidden
            }, 1000, 'swing');
            $("a.displayer[href='#"+next+"']").removeClass("active")
        });
        // Affiche la section
        $('#'+ which).stop().animate({
            left: this.positions[which].visible
        }, 1000, 'swing');

        $("a.displayer[href='#"+which+"']").addClass("active");
    };

    // Au chargement de la page, bootstrap de ce composant
    $(document).ready(function () {
        // Rend le lien "demo" cliquable
        $("#start-button").click(function () {
            ORLANDO.UI.Workspace.changeState("visible-workspace");
        });
        // Rend les liens des sections cliquables.
        $("a.displayer").click(function () {
            var anchor = this.href.substring(this.href.indexOf("#")+1);
            ORLANDO.UI.Workspace.nicelyDisplaySection(anchor);
        });

        // Changement d'état
        ORLANDO.UI.Workspace.changeState("empty");

        // Après un certain temps, affiche les greetings
        setTimeout(function () {
            ORLANDO.UI.Workspace.changeState("greeting")
        },500);

    });
    return that;
}());

/** Movies est le singleton chargé de gérer l'interface pour la partie Spectacle. */
ORLANDO.UI.Movies = (function () {
    "use strict";
    var that, spectacleListItemTemplate, spectacleDetailTemplate;

    that = {};
    spectacleListItemTemplate = "";
    spectacleDetailTemplate = "";

    /** Tri par titre (alpha).*/
    that.orderByTitre = function(anArrayOfSpectacles){
        return _.sortBy(anArrayOfSpectacles,"titre");
    };

    /** Tri par durée.*/
    that.orderByDuration = function(anArrayOfSpectacles){
        console.info("Order by duration");
        return _.sortBy(anArrayOfSpectacles,"duree");
    };

    /** Fonction de tri actuellement à appliquer lors de l'affichage des listes de film.*/
    that.currentOrdering = that.orderByTitre;

    /** Cache la liste des films et la liste 'vide', puis execute la fonction en paramètre.*/
    that.hideSpectaclesListsAndThen = function (doThis) {
        var spectacleLists = $("#spectacle-list"), empty = $("#spectacle-list-empty");
        if (spectacleLists.is(":visible")) {
            spectacleLists.slideUp(function () {
                doThis();
            });
        }
        if (empty.is(":visible")) {
            empty.slideUp(function () {
                doThis();
            });
        }
    };

    /** Affiche un loader, puis execute la fonction en paramètre.*/
    that.showLoadingAndThen = function (doThis) {
        var loading = $("#spectacle-list-loading");
        loading.fadeIn(100, function () {
            doThis();
        });
    };

    /** Cache le loader, puis execute la fonction en paramètre.*/
    that.hideSpinnerAndThen = function (doThis) {
        var loading = $("#spectacle-list-loading");
        loading.stop().fadeOut(100, function () {
            doThis();
        });
    };

    /** Affiche la liste de spectacles vide. */
    that.displayEmptySpectacleList = function () {
        var spectacleListEmpty = $("#spectacle-list-empty");
        spectacleListEmpty.slideDown();
    };

    /** Affiche la liste des spectacles.*/
    that.displaySpectacleList = function () {
        var spectacleList = $("#spectacle-list");
        spectacleList.stop().slideDown();
    };

    /** Remplit la fiche de détail du film a partir du film passé en paramètre.
     * Utilise les fonctionnalités de templating fournit par underscore.js
     **/
    that.updateMovieDetailWith = function (aMovie) {
        if (aMovie == null || aMovie == undefined) {
            $("#spectacle-detail").html( spectacleDetailTemplate({
                id:-1,
                titre : "&lt;Aucun film choisi&gt;",
                dts : "non",
                duree : "0mn",
                img : ""
            })
            );
        } else {
            // load image path from imdb
            // todo move the ajax call to a DAO
            $.ajax({
                url: '/imdb/' + aMovie.titre,
                data: {},
                success: function(data) {
                    // preload image
                    $('<img />').attr('src', data).load(function() {
                        // and display it
                        $("#spectacle-poster").attr("src",data);
                    });
                },
                error : function(a,b,c){
                    alert("Une erreur est survenue pendant le chargement des informations du film sur imdb.");
                },
                dataType: "html"
            });
            // inject into the template the loading spinner img instead of real poster.
            aMovie.img = "/img/ajaxLoader.gif";
            $("#spectacle-detail").html(spectacleDetailTemplate(aMovie));
        }
    };

    /** Met à jour l'affichage en fonction de la liste des films passée en paramètre.*/
    that.updateListWith = function (movieList) {
        var that = this;
        if (movieList == null || movieList.length == 0) {
            this.hideSpinnerAndThen(function(){
                that.displayEmptySpectacleList()
            });
        } else {
            $("#spectacle-list").html(_.map(movieList, spectacleListItemTemplate).join(""));
            // Rend les liens nouvellement créés cliquables.
            $("a.movieLink").click(function () {
                that.updateMovieDetailWith(ORLANDO.DAO.Movies.getMovieById(this.id.substring(this.id.lastIndexOf("-") + 1, this.id.length)));
            });
            this.hideSpinnerAndThen(function(){
                that.displaySpectacleList()
            });
        }
    };

    // Au chargement de la page, bootstrap de ce composant
    $(document).ready(function () {
        // Configuration du système de template d'underscore.js
        _.templateSettings = {
            interpolate : /__(.+?)__/g // ERB style and mustache style doesn't fit in html id's attributes, so let's use __xx__
        };

        var ui = ORLANDO.UI.Movies;
        var nicelyRefreshMovieListFrom = function (thisFunctionReturn) {
            ui.hideSpectaclesListsAndThen(function () {
                ui.showLoadingAndThen(function () {
                    ui.updateListWith(
                        thisFunctionReturn()
                        );
                });
            }
            );
        };
        // Touche entrée pressée => lance une recherche
        $("#filter-by-titre").keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                //console.info(ORLANDO.DAO.Movies.getMoviesWithTitreContaining($("#filter-by-titre").val()))
                nicelyRefreshMovieListFrom( function () {
                    return ORLANDO.UI.Movies.currentOrdering(
                        ORLANDO.DAO.Movies.getMoviesWithTitreContaining($("#filter-by-titre").val())
                        );
                });
            }
        });
        // Click sur l'un des radios boutons => change la fonction de tri courante
        $("input[name='sortBy']").click(function () {
            var value = $("input[name='sortBy']:checked").val();
            if(value == "titre"){
                ORLANDO.UI.Movies.currentOrdering = ORLANDO.UI.Movies.orderByTitre;
            }else
            if(value == "duree"){
                ORLANDO.UI.Movies.currentOrdering = ORLANDO.UI.Movies.orderByDuration
            }
        });

        $("#spectacle-list-reload").click(function () {
            nicelyRefreshMovieListFrom( function () {
                return ORLANDO.UI.Movies.currentOrdering(
                    ORLANDO.DAO.Movies.getAllMovies()
                    );
            });
        });
        // Extraction du code des templates de la page
        spectacleListItemTemplate = _.template($("#spectacle-list").html().trim());
        spectacleDetailTemplate = _.template($("#spectacle-detail").html().trim());
    });

    return that;
}());
