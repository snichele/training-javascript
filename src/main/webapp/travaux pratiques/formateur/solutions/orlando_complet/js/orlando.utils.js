if(typeof ORLANDO === 'undefined') alert("Le fichier 'main' doit être inclus avant 'utils' !");

/**
 * Utils est le namespace contenant les fonctions transverses utilitaires.
 **/
ORLANDO.namespace("Utils");

/**
 * Vérifie pour un objet donné qu'un membre du bon type est bien définit.
 *
 * @param {o} o L'objet à vérifier
 * @param {mName} mName Le nom du membre qui doit être obligatoirement définit
 * @param {type} type Le type du membre qui doit être obligatoirement définit
 * @return undefined
 */
ORLANDO.Utils.mandatoryMemberDefinedOfType = function (o, mName, type) {
    "use strict";
    if (o[mName] === "undefined") {
        throw {
            name: 'MissingMandatoryProperty',
            message: ' the mandatory property' + mName + ' was missing !'
        };
    }
    if (typeof o[mName] !== type) {
        throw {
            name: 'InvalidTypeProperty',
            message: ' the property "' + mName + '" was supposed to be a ' + type
        };
    }
};
