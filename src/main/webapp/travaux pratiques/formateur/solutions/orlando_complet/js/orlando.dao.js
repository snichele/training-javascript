if(typeof ORLANDO === 'undefined') alert("Le fichier 'main' doit être inclus avant 'dao' !");
if(typeof _ === 'undefined') alert("La librairie underscore.js doit être inclus avant 'dao' !");
if(typeof $ === "undefined") alert("La librairie jquery doit être inclus avant 'dao' !");

/**
 * DAO est le namespace contenant les fonctions d'accès aux données externes
 */
ORLANDO.namespace("DAO");

/** Movies est un singleton contenant les méthodes d'accès spécifiques à l'objet Movie.*/
ORLANDO.DAO.Movies = (function () {
    "use strict";
    return {
        getMovieById : function (anId) {
            return _.find(this.getAllMovies(), function (next) {
                return next.id == anId;
            });
        },
        getMoviesWithTitreContaining : function(titreFragment) {
            return _.filter(this.getAllMovies(), function (next) {
                return next.titre.indexOf(titreFragment) != -1;
            });
        },
        getAllMovies : function () {
            var movies;
            $.ajax({
                url: "/spectacles/list",
                dataType: "script",
                async : false,
                success: function (data, textStatus, jqxhr) {
                    movies = eval(data);
                }
            });

            return movies;
        }
    };
}());