test( "Seance manager new seance creation - rules", function() {
    var now = new Date();
    var movie = ORLANDO.Model.newMovie({titre : "toto", duree : 120, dts : true});
    var result1 = ORLANDO.Model.SeancesManager.newSeance(now,ORLANDO.Model.SALLES.UNE,movie,ORLANDO.Model.HORAIRES.H10);
    ok(typeof result1.salle !== "Undefined", "salle property of the seance object is not supposed to be undefined !" );
    var result2 = ORLANDO.Model.SeancesManager.newSeance(now,ORLANDO.Model.SALLES.UNE,movie,ORLANDO.Model.HORAIRES.H10);
    ok(result2 instanceof  ORLANDO.Model.UnavailableHoraireError, "An error must be thrown !" );
});