if(typeof ORLANDO === 'undefined') alert("Le fichier 'main' doit être inclus avant 'model' !");
if(typeof ORLANDO.Utils === 'undefined') alert("Le fichier 'utils' doit être inclus avant 'model' !");

/*
 * Contient la définition des objets 'métier' définit par la librairie.
 */
ORLANDO.namespace("Model");

/**
 * Crée une nouvelle instance de Movie.
 *
 * @param {spec} spec Objet anonyme contenant les propriétés de film à créer (titre, durée et dts)
 * @return un objet Movie
 */
ORLANDO.Model.newMovie = function (spec) {
    "use strict";
    if (spec == null) {
        throw {
            name: 'MissingSpecObject',
            message: ' the spec object for the constructor function newMovie was missing ! '
        };
    }
    var that = spec, check = ORLANDO.Utils;
    that.__proto__ = ORLANDO.Model.newMovie.prototype;
    check.mandatoryMemberDefinedOfType(spec, "titre", "string");
    check.mandatoryMemberDefinedOfType(spec, "duree", "number");
    check.mandatoryMemberDefinedOfType(spec, "dts", "boolean");

    that.dureeInMn = function () {
        return this.duree + " mn";
    };
    return that;
};
ORLANDO.Model.newMovie.prototype.titleFormat = function(str){
    return str;
}
ORLANDO.Model.newMovie.prototype.durationFormat = function(str){
    return "(" + str + ")";
}
ORLANDO.Model.newMovie.prototype.dtsFormat = function(bool){
    return "DTS : " + ((bool)?"Oui":"Non");
}

ORLANDO.Model.newMovie.prototype.getFullyFormattedTitleWith = function(fTitre, fDuree, fDTS){
    return fTitre(this.titre) + fDuree(this.dureeEnMn) + fDTS(this.dts)
}

ORLANDO.Model.SALLES = {
    UNE : {
        places : new Array(20,20)
    },
    DEUX : {
        places : new Array(10,10)
    },
    TROIS : {
        places : new Array(6,7)
    }
}

ORLANDO.Model.HORAIRES = {
    H10 : 1,
    H13 : 2,
    H17 : 3
}

ORLANDO.Model.UnavailableHoraireError = function(){
    this.message = "Horaire indisponible !"
}

ORLANDO.Model.SeancesManager = function(){
    // variable 'privée'
    var availablesHorairesByDateAndSalle = {};

    // Renvoit un objet ayant accès aux variables privées mais ne les exposants pas.
    return {
        newSeance : function(forDay,inSalle,forMovie, atHoraire){
            if(this.isHorairesAvailableForDateAndSalle(forDay,inSalle,atHoraire)){
                this.makeHoraireUnavailableForDateAndSalle(forDay,inSalle,atHoraire)
                return {
                    day : forDay,
                    salle : inSalle,
                    movie : forMovie,
                    horaire : atHoraire
                }
            }else{
                return new ORLANDO.Model.UnavailableHoraireError();
            }
        },

        /** Interroge le tableau de référence, l'initialize si nécessaire (lazy initialisation)*/
        getHorairesByDatePossiblyInitializingIt : function(aDate){
            if(availablesHorairesByDateAndSalle[aDate] == null) {
                console.warn("No entry for date " + aDate)
                console.info("Let's create some !")
                availablesHorairesByDateAndSalle[aDate] = {}
                for(var S in ORLANDO.Model.SALLES){
                    console.info("Adding salle " + ORLANDO.Model.SALLES[S])
                    availablesHorairesByDateAndSalle[aDate][ORLANDO.Model.SALLES[S]] = {}
                    for(var H in ORLANDO.Model.HORAIRES){
                        console.info("Adding horaire " + ORLANDO.Model.HORAIRES[H])
                        availablesHorairesByDateAndSalle[aDate][ORLANDO.Model.SALLES[S]][ORLANDO.Model.HORAIRES[H]] = true
                    }
                }
            }
            return availablesHorairesByDateAndSalle[aDate];
        },
        makeHoraireUnavailableForDateAndSalle : function(aDate,aSalle,atHoraire){
            this.getHorairesByDatePossiblyInitializingIt(aDate)[aSalle][atHoraire] = false;
        },
        isHorairesAvailableForDateAndSalle : function(aDate,aSalle,atHoraire){
            return this.getHorairesByDatePossiblyInitializingIt(aDate)[aSalle][atHoraire];
        }
    }
}();
