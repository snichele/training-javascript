/* Namespaces declaration managment.
 * Borrowed from YAHOO
 *   */
var ORLANDO = ORLANDO || {};
var $ = $ || {};
var _ = _ || {};
var console = console || {};

ORLANDO.namespace = function () {
    "use strict";
    var a = arguments, o = null, i, j, d;
    for (i = 0; i < a.length; i = i + 1) {
        d = ("" + a[i]).split(".");
        o = ORLANDO;
        for (j = (d[0] === "ORLANDO") ? 1 : 0; j < d.length; j = j + 1) {
            o[d[j]] = o[d[j]] || {};
            o = o[d[j]];
        }
    }
    return o;
};